﻿using DummyDatabase.Console;
using System.Data;

namespace DummyDatabase.Core
{
    public class LinkedTablesExamination
    {
        public static Error? IsLinkedTablesValid(Table table)
        {
            for (int i = 0; i < table.Scheme.Columns.Count; i++)
            {
                SchemeColumn column = table.Scheme.Columns[i];
                if (column.IsPrimaryKey)
                {
                    foreach (Link foreignTableLink in column.ForeignTablesLinks)
                    {
                        object obj = TryGetLinkedTable(foreignTableLink, table);
                        if (obj is Table foreignTable)
                        {
                            List<Link> foreignTablesLinksList = column.ForeignTablesLinks.ToList();
                            string foreignColName = foreignTablesLinksList[i].ColumnName;
                            string tableName = foreignTable.Name;
                            foreach (SchemeColumn foreignTableColumn in foreignTable.Scheme.Columns)
                                if (foreignTableColumn.PrimaryTable == table)
                                    if (foreignTable.Scheme.Columns[foreignTableColumn.ForeignColumnIndex].Name != foreignColName)
                                        return new Error((int)ErrorsDisplayer.ErrorIndices.LinkedTableColNameExistenceIndex, tableName,
                                                        foreignTable.TableFileInfo.Name, foreignColName);
                                    else break;

                            Error? foreignTableError = IsForeignKeyColValuesValid(tableName, table, foreignTable, column.Name);
                            if (foreignTableError != null)
                                return foreignTableError;
                            else
                                column.ForeignTables.Add(foreignTable);
                        }
                        else if (obj is Error foreignTableError)
                        {
                            return foreignTableError;
                        }
                    }
                }
                else if (column.IsForeignKey)
                {
                    object obj = TryGetLinkedTable(column.PrimaryTableLink, table);
                    if (obj is Table primaryTable)
                    {
                        string primaryColName = column.PrimaryTableLink.ColumnName;
                        string tableName = primaryTable.Name;
                        if (primaryTable.Scheme.Columns[primaryTable.PrimaryColumnIndex].Name != primaryColName)
                            return new Error((int)ErrorsDisplayer.ErrorIndices.LinkedTableColNameExistenceIndex, tableName,
                                                    primaryTable.TableFileInfo.Name, primaryColName);
                        Error? foreignKeyColValuesError = IsForeignKeyColValuesValid(tableName, primaryTable, table, primaryColName);
                        if (foreignKeyColValuesError != null)
                            return foreignKeyColValuesError;
                        else
                            column.PrimaryTable = primaryTable;
                    }
                    else if (obj is Error primaryTableError)
                    {
                        return primaryTableError;
                    }
                }
            }
            return null;
        }

        public static Error? IsForeignKeyColValuesValid(string tableName, Table primaryTable, Table foreignTable, string primaryColName)
        {
            foreach (DataRow row in foreignTable.DataTable.Rows)
            {
                if (!primaryTable.PrimaryElemsList.Contains(row[primaryTable.PrimaryColumnIndex].ToString()))
                    return new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryKeyColValueExistenceIndex, tableName,
                                        primaryTable.TableFileInfo.Name, primaryColName, row[primaryTable.PrimaryColumnIndex]);
            }
            return null;
        }

        private static object TryGetLinkedTable(Link linkedTableLink, Table selectedTable)
        {
            Error? linkedTableLinkError = IsLinkedTableLinkValid(linkedTableLink, selectedTable);
            if (linkedTableLinkError == null)
            {
                Table linkedTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[linkedTableLink.FileName]];
                Error? linkedTableInitializeError = TableInitializer.TryToInitializeTheTable(linkedTable);
                if (linkedTableInitializeError != null)
                    return linkedTableInitializeError;
                else
                    return linkedTable;
            }
            else
            {
                return linkedTableLinkError;
            }
        }

        private static Error? IsLinkedTableLinkValid(Link linkedTableLink, Table selectedTable)
        {
            if (!File.Exists(linkedTableLink.FilePath))
            {
                return new Error((int)ErrorsDisplayer.ErrorIndices.LinkedFileExistenceIndex, selectedTable.Name, linkedTableLink.FilePath);
            }
            else
            {
                FileInfo fileInfo = new FileInfo(linkedTableLink.FilePath);
                linkedTableLink.FileName = fileInfo.Name;
            }

            if (!Directory.GetFiles(selectedTable.Scheme.DatabasePath, linkedTableLink.FileName, SearchOption.AllDirectories).Any())
                return new Error((int)ErrorsDisplayer.ErrorIndices.LinkedFileExistenceInDatabaseIndex, selectedTable.Name, linkedTableLink.FilePath);
            return null;
        }
    }
}
