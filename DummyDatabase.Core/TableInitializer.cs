﻿using DummyDatabase.Console;
using System.Data;
using System.Globalization;
using System.Text;

namespace DummyDatabase.Core
{
    public class TableInitializer
    {
        public static List<Table> Tables { get; set; } = new List<Table>();
        public static Dictionary<string, int> FromFileNameToTableListIndex { get; set; } = new Dictionary<string, int>();

        public static Error? TryToInitializeTheTable(Table table)
        {
            if (!table.IsInitialized)
            {
                if (!File.Exists(table.SchemeFileInfo.FullName))
                    return new Error((int)ErrorsDisplayer.ErrorIndices.SchemeExistenceIndex, table.Name, table.SchemeFileInfo.FullName);

                string[] linesArray = File.ReadAllLines(table.TableFileInfo.FullName);
                List<StringBuilder> sbLines = new List<StringBuilder>();
                foreach (string line in linesArray)
                {
                    StringBuilder sb = new StringBuilder(line);
                    sbLines.Add(sb);
                }

                object obj = Scheme.TryGetTheScheme(table.SchemeFileInfo.FullName, table.Name);
                if (obj is Scheme)
                {
                    table.Scheme = obj as Scheme;
                    Error? schemeStructureError = table.Scheme.IsTheSchemeValid(table.Name);
                    if (schemeStructureError != null)
                        return schemeStructureError;

                    if (!Directory.Exists(table.Scheme.DatabasePath))
                        return new Error((int)ErrorsDisplayer.ErrorIndices.DatabaseExistenceIndex, table.Name, table.Scheme.DatabasePath);
                }
                else
                {
                    return obj as Error;
                }

                Error? approptiationError = DataExamination.IsAppropriateToScheme(table.Scheme, sbLines, table.Name);
                if (approptiationError != null)
                    return approptiationError;

                Error? dataTableError = TryGetTheDataTable(table, sbLines, table.Scheme);
                if (dataTableError != null)
                    return dataTableError;

                table.IsInitialized = true;
                Error? linkedTablesError = LinkedTablesExamination.IsLinkedTablesValid(table);
                if (linkedTablesError != null)
                {
                    table.IsInitialized = false;
                    return linkedTablesError;
                }

                for (int i = 0; i < table.Scheme.Columns.Count; i++)
                {
                    SchemeColumn column = table.Scheme.Columns[i];
                    if (column.IsForeignKey)
                        GetTheUsingPrimaryValues(column.PrimaryTable, table, i);
                }
            }

            return null;
        }

        public static void GetTheUsingPrimaryValues(Table primaryTable, Table table, int foreignKeyColIndex)
        {
            primaryTable.UsedPrimaryElemsList = new List<string>();
            foreach (DataRow row in table.DataTable.Rows)
                primaryTable.UsedPrimaryElemsList.Add(row[foreignKeyColIndex].ToString());
        }

        private static Error? TryGetTheDataTable(Table table, List<StringBuilder> lines, Scheme scheme)
        {
            DataTable dataTable = new DataTable();
            Dictionary<string, int> fromColumnNameToColumnIndex = new Dictionary<string, int>();
            int primaryKeysCount = 0;

            for (int i = 0; i < scheme.Columns.Count; i++)
            {
                SchemeColumn column = scheme.Columns[i];
                string typeName = SchemeColumn.TypeAbbreviations[column.Type];
                Type type = Type.GetType(typeName);

                DataColumn dataColumn = new DataColumn();
                dataColumn.ColumnName = column.Name;
                fromColumnNameToColumnIndex.Add(column.Name, i);
                dataColumn.DataType = type;
                if (column.IsPrimaryKey)
                {
                    if (column.IsForeignKey)
                        return new Error((int)ErrorsDisplayer.ErrorIndices.KeysContradictionIndex, table.Name);
                    else if (primaryKeysCount++ == 0)
                        dataColumn.Unique = true;
                    else
                        return new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryKeysCountIndex, table.Name);
                    table.PrimaryColumnIndex = i;
                }
                else if (column.IsForeignKey)
                {
                    column.ForeignColumnIndex = i;
                }

                dataTable.Columns.Add(dataColumn);
            }

            for (int i = 0; i < lines.Count; i++)
            {
                string[] lineElements = lines[i].ToString().Split(";");

                List<object> row = new List<object>();
                for (int j = 0; j < lineElements.Length; j++)
                {
                    row.Add(GetTheValue(lineElements[j], scheme.Columns[j]));
                    if (table.PrimaryColumnIndex == j)
                        table.PrimaryElemsList.Add(row[^1].ToString());
                }

                try
                {
                    dataTable.Rows.Add(row.ToArray());
                }
                catch (ConstraintException)
                {
                    return new Error((int)ErrorsDisplayer.ErrorIndices.NotUniqueValuesIndex, table.Name);
                }
            }

            table.DataTable = dataTable;
            return null;
        }

        public static object GetTheValue(string value, SchemeColumn column)
        {
            string dateFormat = "dd.MM.yyyy";
            string dateTimeFormat = "dd.MM.yyyy HH:mm:ss";
            CultureInfo provider = CultureInfo.InvariantCulture;

            return column.Type switch
            {
                "char" => char.Parse(value),
                "int" => int.Parse(value),
                "long" => long.Parse(value),
                "float" => float.Parse(value),
                "double" => double.Parse(value),
                "decimal" => decimal.Parse(value),
                "bool" => bool.Parse(value),
                "dateTime" => DateTime.ParseExact(value, dateTimeFormat, provider),
                "date" => DateTime.ParseExact(value, dateFormat, provider),
                _ => value,
            };
        }
    }
}
