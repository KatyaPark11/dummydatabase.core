﻿using DummyDatabase.Console;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace DummyDatabase.Core
{
    public class Scheme
    {
        [JsonProperty(PropertyName = "columns")]
        public ObservableCollection<SchemeColumn> Columns = new();

        [JsonProperty(PropertyName = "databasePath")]
        public string DatabasePath { get; private set; }

        public static object TryGetTheScheme(string path, string tableName)
        {
            try 
            {
                return JsonConvert.DeserializeObject<Scheme>(File.ReadAllText(path));
            }
            catch 
            {
                return new Error((int)ErrorsDisplayer.ErrorIndices.JsonFileIndex, tableName);
            }
        }

        public Error? IsTheSchemeValid(string tableName)
        {
            if (this.DatabasePath == null)
            {
                return new Error((int)ErrorsDisplayer.ErrorIndices.SchemeElemsCountIndex);
            }

            foreach (SchemeColumn column in this.Columns)
            {
                if (column.Name == null || column.Type == null)
                {
                    return new Error((int)ErrorsDisplayer.ErrorIndices.SchemeElemsCountIndex);
                }
                else if (column.IsForeignKey && (column.PrimaryTableLink == null || String.IsNullOrEmpty(column.PrimaryTableLink.FilePath) || 
                         column.PrimaryTableLink.FilePath == "null" || String.IsNullOrEmpty(column.PrimaryTableLink.ColumnName) || column.PrimaryTableLink.ColumnName == "null"))
                {
                    return new Error((int)ErrorsDisplayer.ErrorIndices.ForeignLinkAbsenceIndex, tableName);
                }
                else if (column.IsPrimaryKey)
                {
                    if (column.ForeignTablesLinks == null)
                        return new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryLinkAbsenceIndex, tableName);
                    foreach (Link foreignTable in column.ForeignTablesLinks)
                        if (String.IsNullOrEmpty(foreignTable.FilePath) || foreignTable.FilePath == "null" ||
                            String.IsNullOrEmpty(foreignTable.ColumnName) || foreignTable.ColumnName == "null")
                            return new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryLinkAbsenceIndex, tableName);
                }
            }
            return null;
        }
    }
}