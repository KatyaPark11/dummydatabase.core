﻿using Newtonsoft.Json;
using System.Data;
using System.Text;

namespace DummyDatabase.Core
{
    public class DataSaving
    {
        private const int dayAndMonthLenInDate = 2;
        private const int yearLenInDate = 4;
        private const int hmsLenInDate = 2;
        public static void SaveDataInFile(Table table)
        {
            string newSchemeText = JsonConvert.SerializeObject(table.Scheme, Formatting.Indented | Formatting.None);
            File.WriteAllText(table.SchemeFileInfo.FullName, newSchemeText);
            UpdateLinkedTablesSchemes(table);
            StringBuilder newFileText = GetNewFileText(table);
            File.WriteAllText(table.TableFileInfo.FullName, newFileText.ToString());
        }

        private static void UpdateLinkedTablesSchemes(Table table)
        {
            string newSchemeText;
            foreach (SchemeColumn column in table.Scheme.Columns)
            {
                if (column.IsPrimaryKey)
                {
                    foreach (Table foreignTable in column.ForeignTables)
                    {
                        newSchemeText = JsonConvert.SerializeObject(foreignTable.Scheme, Formatting.Indented | Formatting.None);
                        File.WriteAllText(foreignTable.SchemeFileInfo.FullName, newSchemeText);
                    }
                }
                else if (column.IsForeignKey)
                {
                    newSchemeText = JsonConvert.SerializeObject(column.PrimaryTable.Scheme, Formatting.Indented | Formatting.None);
                    File.WriteAllText(column.PrimaryTable.SchemeFileInfo.FullName, newSchemeText);
                }
            }
        }

        private static StringBuilder GetNewFileText(Table table)
        {
            StringBuilder newFileText = new StringBuilder();
            foreach (DataRow row in table.DataTable.Rows)
            {
                string newRow = "";
                foreach (SchemeColumn column in table.Scheme.Columns)
                {
                    if (column.Type == "date")
                    {
                        DateTime dateTime = DateTime.Parse(row[column.Name].ToString());
                        newRow += $"{dateTime.Day.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                  $"{dateTime.Month.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                  $"{dateTime.Year.ToString().PadLeft(yearLenInDate, '0')};";
                    }
                    else if (column.Type == "dateTime")
                    {
                        DateTime dateTime = DateTime.Parse(row[column.Name].ToString());
                        newRow += $"{dateTime.Day.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                  $"{dateTime.Month.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                  $"{dateTime.Year.ToString().PadLeft(yearLenInDate, '0')}" +
                                  $" {dateTime.Hour.ToString().PadLeft(hmsLenInDate, '0')}:" +
                                  $"{dateTime.Minute.ToString().PadLeft(hmsLenInDate, '0')}:" +
                                  $"{dateTime.Second.ToString().PadLeft(hmsLenInDate, '0')};";
                    }
                    else
                    {
                        newRow += $"{row[column.Name]};";
                    }
                }
                newFileText.AppendLine(newRow.Remove(newRow.Length - 1));
            }
            return newFileText;
        }
    }
}