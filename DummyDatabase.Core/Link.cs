﻿using Newtonsoft.Json;

namespace DummyDatabase.Core
{
    public class Link
    {
        [JsonProperty(PropertyName = "filePath")]
        public string FilePath { get; set; }

        [JsonIgnore]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "columnName")]
        public string ColumnName { get; set; }
    }
}
