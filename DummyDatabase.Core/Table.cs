﻿using DummyDatabase.Console;
using System.Data;
using System.Globalization;
using System.Text;

namespace DummyDatabase.Core
{
    public class Table
    {
        public bool IsInitialized { get; set; }
        public string Name { get; set; }
        public DataTable DataTable { get; set; }
        public Scheme Scheme { get; set; }
        public FileInfo TableFileInfo { get; set; }
        public FileInfo SchemeFileInfo { get; set; }
        public int PrimaryColumnIndex { get; set; } = -1;
        public List<string> PrimaryElemsList { get; set; } = new List<string>();
        public List<string> UsedPrimaryElemsList { get; set; }

        public Table(FileInfo tableFileInfo, FileInfo schemeFileInfo)
        {
            TableFileInfo = tableFileInfo;
            Name = this.TableFileInfo.Name;
            SchemeFileInfo = schemeFileInfo;
        }
    }
}