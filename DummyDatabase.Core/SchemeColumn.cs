﻿using Newtonsoft.Json;

namespace DummyDatabase.Core
{
    public class SchemeColumn
    {
        public static readonly Dictionary<string, string> TypeAbbreviations = new Dictionary<string, string>()
        {
            { "int", "System.Int32" },
            { "long", "System.Int64" },
            { "bool", "System.Boolean" },
            { "char", "System.Char" },
            { "decimal", "System.Decimal" },
            { "double", "System.Double" },
            { "float", "System.Single" },
            { "string", "System.String" },
            { "dateTime", "System.DateTime" },
            { "date", "System.DateTime" }
        };

        public static readonly Dictionary<string, object> DefaultValues = new Dictionary<string, object>()
        {
            { "System.Int32", default(int) },
            { "System.Int64", default(long) },
            { "System.Boolean", default(bool) },
            { "System.Char", default(char) },
            { "System.Decimal", default(decimal) },
            { "System.Double", default(double) },
            { "System.Single", default(float) },
            { "System.String", string.Empty },
            { "System.DateTime", default(DateTime) }
        };

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "isPrimaryKey")]
        public bool IsPrimaryKey { get; set; }

        [JsonProperty(PropertyName = "foreignTables")]
        public HashSet<Link> ForeignTablesLinks { get; set; } = new HashSet<Link>();

        [JsonIgnore]
        public List<Table> ForeignTables { get; set; } = new List<Table>();

        [JsonProperty(PropertyName = "isForeignKey")]
        public bool IsForeignKey { get; set; }

        [JsonIgnore]
        public int ForeignColumnIndex { get; set; }

        [JsonProperty(PropertyName = "primaryTable")]
        public Link PrimaryTableLink { get; set; }

        [JsonIgnore]
        public Table PrimaryTable { get; set; }
    }
}