﻿using DummyDatabase.Console;
using System.Globalization;
using System.Text;

namespace DummyDatabase.Core
{
    public class DataExamination
    {
        public static Error? IsAppropriateToScheme(Scheme scheme, List<StringBuilder> table, string tableName)
        {
            Error? countError = IsRightCountOfColumns(table, scheme, tableName);
            if (countError != null)
                return countError;

            Error? typeError = IsRightTypesOfObjects(table, scheme, tableName);
            if (typeError != null)
                return typeError;

            return null;
        }

        public static Error? IsRightCountOfColumns(List<StringBuilder> lines, Scheme scheme, string tableName)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                string[] elements = lines[i].ToString().Split(";");
                int elementsCount = elements.Length;
                int rightCount = scheme.Columns.Count;

                if (elementsCount != rightCount)
                    return new Error((int)ErrorsDisplayer.ErrorIndices.CountOfElementsIndex, tableName, i + 1, rightCount, elementsCount);
            }

            return null;
        }

        public static Error? IsRightTypesOfObjects(List<StringBuilder> lines, Scheme scheme, string tableName)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                string[] elements = lines[i].ToString().Split(";");
                for (int j = 0; j < elements.Length; j++)
                {
                    string element = elements[j];
                    string type = scheme.Columns[j].Type;
                    bool res = IsRightType(element, type);

                    if (!res)
                        return new Error((int)ErrorsDisplayer.ErrorIndices.ColumnTypeIndex, tableName, i + 1, j + 1, element, type);
                }
            }

            return null;
        }

        public static bool IsRightType(string element, string type)
        {
            bool res = true;

            switch (type)
            {
                case "char":
                    res = char.TryParse(element, out _);
                    break;
                case "int":
                    res = Int32.TryParse(element, out _);
                    break;
                case "long":
                    res = long.TryParse(element, out _);
                    break;
                case "float":
                    res = float.TryParse(element, out _);
                    break;
                case "double":
                    res = double.TryParse(element, out _);
                    break;
                case "decimal":
                    res = decimal.TryParse(element, out _);
                    break;
                case "bool":
                    res = bool.TryParse(element, out _);
                    break;
                case "dateTime":
                    string dateTimeFormat = "dd.MM.yyyy HH:mm:ss";
                    CultureInfo provider1 = CultureInfo.InvariantCulture;
                    res = DateTime.TryParseExact(element, dateTimeFormat, provider1, DateTimeStyles.None, out _);
                    break;
                case "date":
                    string dateFormat = "dd.MM.yyyy";
                    CultureInfo provider2 = CultureInfo.InvariantCulture;
                    res = DateTime.TryParseExact(element, dateFormat, provider2, DateTimeStyles.None, out _);
                    break;
            }

            return res;
        }
    }
}